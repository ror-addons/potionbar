if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.CurrentMajorVersion = 7
PotionBar.CurrentMinorVersion = 2

PotionBar.LibSlashCommand = nil

PotionBar.Type = {}
PotionBar.Type =
{
    [1] = { Name = "AP", 										Desc = L"AP Potions" },														
		[2] = { Name = "AP_RVR", 								Desc = L"AP Potions (RvR)" },
    [3] = { Name = "HEAL", 									Desc = L"Heal Potions" },
		[4] = { Name = "HEAL_RVR", 							Desc = L"Heal Potions (RvR)" },
    [5] = { Name = "REGEN", 								Desc = L"Regen Potions"},
    [6] = { Name = "SHIELD", 								Desc = L"Damage Shield"},
    [7] = { Name = "STRENGTH", 							Desc = L"Strength" },
    [8] = { Name = "INTELLIGENCE", 					Desc = L"Intelligence" },
    [9] = { Name = "WILLPOWER", 						Desc = L"Willpower" },
    [10] = { Name = "BALLISTIC", 						Desc = L"Ballistic" },
    [11] = { Name = "ARMOR", 								Desc = L"Armor" },
    [12] = { Name = "TOUGHNESS", 						Desc = L"Toughness" },
    [13] = { Name = "THORNS", 							Desc = L"Thorns" },
    [14] = { Name = "SPIRIT", 							Desc = L"Spiritual" },
    [15] = { Name = "ELEMENTAL", 						Desc = L"Elemental" },
    [16] = { Name = "CORPOREAL", 						Desc = L"Corporeal" },
    [17] = { Name = "DOT", 									Desc = L"Burning" },
    [18] = { Name = "AOEDOT", 							Desc = L"Explosive" },
    [19] = { Name = "FIREBREATH", 					Desc = L"Firebreath" },
    [20] = { Name = "SNARE", 								Desc = L"Snaring" },
    [21] = { Name = "WARBLOOD", 						Desc = L"Wounds + Melee Crit" }, 												-- Liniment: War Blood
    [22] = { Name = "WARDEMISE", 						Desc = L"Wounds + Melee Power" }, 											-- Liniment: War Demise
    [23] = { Name = "WARFERVOR", 						Desc = L"Wounds + Ranged Crit" }, 											-- Liniment: War Fervor
    [24] = { Name = "WARGENIUS", 						Desc = L"Wounds + Magic Crit" }, 												-- Liniment: War Genius
    [25] = { Name = "WARHUNGER", 						Desc = L"Wounds + Strength" }, 													-- Liniment: War Hunger
    [26] = { Name = "WARMERCY", 						Desc = L"Wounds + Healing Power" }, 										-- Liniment: War Mercy
    [27] = { Name = "BOUNDLESSSIGHT", 			Desc = L"Wounds + Initiative" }, 												-- Liniment: Boundless Sight
    [28] = { Name = "IMMUTABLEDEFIANCE", 		Desc = L"Health Regen + Reduced Crit Dmg" }, 						-- Liniment: Immutable Defiance
    [29] = { Name = "INEXORABLEAEGIS", 			Desc = L"Resists +180" }, 															-- Liniment: Inexorable Aegis
    [30] = { Name = "INSPIRATIONALWINDS", 	Desc = L"Willpower + Healing Power" }, 									-- Liniment: Inspirational Winds
    [31] = { Name = "PEERLESSDEFENSE", 			Desc = L"Toughness + Reduced Crit Hit Chance" }, 				-- Liniment: Peerless Defense
    [32] = { Name = "QUICKENEDBLADES", 			Desc = L"Weapon Skill + Reduced Armor Pen" },						-- Liniment: Quickened Blades
    [33] = { Name = "SAVAGEVIGOR", 					Desc = L"Toughness + Melee Crit" }, 										-- Liniment: Savage Vigor
    [34] = { Name = "SWIFTTERGIVERSATION", 	Desc = L"Weapon Skill + Reduced Crit Hit Chance" },			-- Liniment: Swift Tergiversation
    [35] = { Name = "ETERNALHUNT", 					Desc = L"Ballistic + Ranged Crit" }, 										-- Liniment: Eternal Hunt
    [36] = { Name = "INEVITABLETEMPEST", 		Desc = L"Strength + Melee Power" }, 										-- Liniment: Inevitable Tempest
		[37] = { Name = "TOLLINGBELL", 					Desc = L"Intelligence + Magic Crit" }, 									-- Liniment: Tolling Bell
    [38] = { Name = "UNFETTEREDZEAL", 			Desc = L"Strength + Healing Power" }, 									-- Liniment: Unfettered Zeal
    [39] = { Name = "NEPENTHEANTONIC", 			Desc = L"Nepenthean Tonic" },
    [40] = { Name = "ZEPHYRTONIC", 					Desc = L"Zephyr Tonic" },
    [41] = { Name = "TAHOTHELIXIR", 				Desc = L"Elixir of Tahoth" },
    [42] = { Name = "PTRAELIXIR",	 					Desc = L"Elixir of Ptra" },
    [43] = { Name = "SPEEDBLOWINGSAND", 		Desc = L"Speed of the Blowing Sand" },
    [44] = { Name = "UNKNOWNS", 						Desc = L"All other" },
    [45] = { Name = "SINGLES", 							Desc = L"Singles" },
}

for i,v in ipairs(PotionBar.Type) do PotionBar.Type[v.Name] = i end

PotionBar.Priorities = {}
PotionBar.Priorities =
{
    BigStack = 1,
    LessStack = 2,
    Weakest = 3,
    Strongest = 4,

    [1] = { Desc = L"Biggest stack" },
    [2] = { Desc = L"Smallest stack" },
    [3] = { Desc = L"Weakest potion" },
    [4] = { Desc = L"Strongest potion" },
}

PotionBar.Build = {}
PotionBar.Build =
{
    Right = 1,
    Left = 2,
    Up = 3,
    Down = 4,

    [1] = { Desc = L"Right" },
    [2] = { Desc = L"Left" },
    [3] = { Desc = L"Up" },
    [4] = { Desc = L"Down" },
}

PotionBar.StackCountInfoType =
{
    Hide = 1,
    Single = 2,
    Total = 3,

    [1] = { Desc = L"Don't show stackcount" },
    [2] = { Desc = L"Show stack count" },
    [3] = { Desc = L"Show total count" },
}

PotionBar.ActivatorModes = 
{
    LeftOrUpper = 1,
    RightOrBottom = 2,
    Hide = 3,

    [1] = { Desc = L"Show Activator" },
    [2] = { Desc = L"Flip Position" },
    [3] = { Desc = L"Hide Activator" },
}

PotionBar.QuickActions = {}
PotionBar.QuickActions =
{
    SelectAll = 1,
    UnSelectAll = 2,
    SelectLiniments = 3,
    UnSelectLiniments = 4,
    SelectStackVariants = 5,
    UnSelectStackVariants = 6,
    ResetPotionDefaults = 7,

    [1] = { Desc = L"Select All For Display" },
    [2] = { Desc = L"UnSelect All For Display" },
    [3] = { Desc = L"Select All Liniments for Display" },
    [4] = { Desc = L"UnSelect All Liniments for Display" },
    [5] = { Desc = L"Select All for Stack All Variants" },
    [6] = { Desc = L"UnSelect All for Stack All Variants" },
    [7] = { Desc = L"Reset All Potions to Default Settings" },
}

PotionBar.QuickActionsSeperators = {}
PotionBar.QuickActionsSeperators =
{
		Alpha = 0.6,
		OffsetXPos = 2,
		OffsetYPos = 2,
		WidthAdjust = 16,
		Height = 10,
		
		-- seperator number --> button reference number
    [1] = 2,
    [2] = 4,
    [3] = 6,
}

PotionBar.LocalizableStrings = {}
PotionBar.LocalizableStrings =
{
    Config = L"Configuration...",
    FloatHint1 = L"Left click and drag to move.",
    FloatHint2 = L"Right click for options.",

    TitleBarText = L"PotionBar Config Window",
    RightPaneTitle = L"Build Order and Priority",
    QuickActionsLabel = L" ---------------- Quick Actions ---------------- ",
    LeftTopPaneTitle = L"Bar Settings",
    BuildLabel = L"Build Direction",
    AutohideLabel = L"Autohide",
    ScaleLabel = L"Scale",
    OpacityLabel = L"Opacity",
    SaveButton = L"Save",
    OkButton = L"OK",
    ResetButton = L"Defaults",
    AboutButton = L"About...",
    InfoTopRight = L"Top Right Count",
    InfoBottomRight = L"Bottom Right Count",
    Show1Label = L"Show Count of 1",
    ShowLastLabel = L"Show Last Stack",
    DontSplitNameLabel = L"Stack All Variants",
    ActivatorLabel = L"Floating Activator Position",

    BuildHint = L"Choose the direction of the bar.",
    AutoHideHint = L"Check this box to make the bar automatically hide.",
    MoveUpHint = L"Click here to move this item upwards in creation order.",
    MoveDownHint = L"Click here to move this item downwards in creation order.",
    UseHint = L"Check this box to show potions of this kind in PotionBar.",
    MethodHint = L"Choose which potions of this type PotionBar should give priority to.",
    InfoTopRightHint = L"Choose the count you want to see in the top right corner of the potion button.",
    InfoBottomRightHint = L"Choose the count you want to see in the bottom right corner of the potion button.",
    Show1Hint = L"Check this box to show stack count even it is 1.",
    ShowLastHint = L"Check this box to show the total stack count, even it is the last stack.",
    QuickActionsHint = L"Allows quick hiding and display of all or select groups of potions/liniments.",
    DontSplitNameHint = L"Check this box if you want all potions of this type as one button",
    ActivatorHint = L"Choose the position of the small round button of PotionBar",

    HelpTitle = L"PotionBar knows the following commands:",
    HelpForUnknown = L"Use this command to drink a specific potion:",
    HelpNameOfPotion = "Name of Potion",
    HelpForMacros = L"If you want to use them from within macros, your macro must look like this:",

    MacroPotionFound1 = "PotionBar is using ",
    MacroPotionFound2 = " out of inventory slot ",
    MacroPotionFound3 = ".",

    MacroNoPotionFound1 = L"PotionBar 'use' command error",
    MacroNoPotionFound2 = "Could not find: ",
    MacroNoPotionFound3 = "",

    MacroPotionOnCoolDown1 = "PotionBar could not use '",
    MacroPotionOnCoolDown2 = "' because it is still on cool down.",
}

function PotionBar.Defaults()
    PotionBar.Settings = {}
    PotionBar.Settings.Buttons = {}
    PotionBar.Settings.Autohide = false
    PotionBar.Settings.Build = PotionBar.Build.Right
    PotionBar.Settings.Opacity = 1
    PotionBar.Settings.Scale = 0.5
    PotionBar.Settings.TopRight = PotionBar.StackCountInfoType.Total
    PotionBar.Settings.BottomRight = PotionBar.StackCountInfoType.Hide
    PotionBar.Settings.ShowStackSizeEvenIf1 = false
    PotionBar.Settings.ShowStackSizeEvenIfLast = false
    PotionBar.Settings.Activator = PotionBar.ActivatorModes.LeftOrUpper
    
    PotionBar.DefaultsPotions()
    
    PotionBar.Settings.Version = tonumber(PotionBar.CurrentMajorVersion..L"."..PotionBar.CurrentMinorVersion)
end

function PotionBar.DefaultsPotions()

    for PotionBarType = 1, #PotionBar.Type do
        PotionBar.Settings.Buttons[PotionBarType] = {}
        PotionBar.Settings.Buttons[PotionBarType].Preference = PotionBar.Priorities.Strongest
        PotionBar.Settings.Buttons[PotionBarType].Use = true
        PotionBar.Settings.Buttons[PotionBarType].Type = PotionBarType
        PotionBar.Settings.Buttons[PotionBarType].SplitName = false
    end

    PotionBar.Settings.Buttons[PotionBar.Type.WARBLOOD].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.WARDEMISE].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.WARFERVOR].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.WARGENIUS].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.WARHUNGER].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.WARMERCY].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.BOUNDLESSSIGHT].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.IMMUTABLEDEFIANCE].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.INEXORABLEAEGIS].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.INSPIRATIONALWINDS].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.PEERLESSDEFENSE].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.QUICKENEDBLADES].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.SAVAGEVIGOR].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.SWIFTTERGIVERSATION].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.ETERNALHUNT].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.INEVITABLETEMPEST].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.TOLLINGBELL].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.UNFETTEREDZEAL].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.NEPENTHEANTONIC].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.ZEPHYRTONIC].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.TAHOTHELIXIR].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.PTRAELIXIR].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.SPEEDBLOWINGSAND].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.UNKNOWNS].Preference = PotionBar.Priorities.LessStack
    PotionBar.Settings.Buttons[PotionBar.Type.UNKNOWNS].SplitName = true
    PotionBar.Settings.Buttons[PotionBar.Type.SINGLES].Use = false
    PotionBar.Settings.Buttons[PotionBar.Type.SINGLES].SplitName = true

end