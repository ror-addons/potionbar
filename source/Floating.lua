if not PotionBar then PotionBar = {} end
if not PotionBarFloating then PotionBarFloating = {} end

PotionBarFloating.Buttons = {}

local isMoving = false
local noBackground = true

local pairs = pairs
local WindowSetAlpha = WindowSetAlpha
local LabelSetText = LabelSetText

function PotionBarFloating.MouseOver()
    if isMoving then return end

    local mouseOverWindow = SystemData.MouseOverWindow.name
    local buttonIndex = tonumber(string.sub(mouseOverWindow, string.find(mouseOverWindow, '%d+')))
    local itemData = PotionBarFloating.Buttons[buttonIndex].itemData

    if itemData == nil or itemData.id == 0 then
        Tooltips.ClearTooltip()
    else
        local actionText, textColor, callback, anchorWindow = nil, nil, nil, mouseOverWindow
        Tooltips.CreateItemTooltip(itemData, anchorWindow, Tooltips.ANCHOR_WINDOW_LEFT, Tooltips.ENABLE_COMPARISON, actionText, textColor, callback)
        Tooltips.SetUpdateCallback(callback)
        if Cursor.UseItemTargeting then
            UseItemTargeting.HandleMouseOverItem(itemData)
        end
    end
end

function PotionBarFloating.MouseOverEnd(buttonId, flags) end
function PotionBarFloating.LButtonUp() SendUseItem(GameData.ItemLocs.INVENTORY, PotionBarFloating.Buttons[PotionBar.getNumberFromWindowName()].slotNum, 0, 0, 0) end

function PotionBarFloating.UsePotion(potionBarType, potionName)
    if potionBarType == nil then return end
    if potionName == nil then potionName = "" end
    for buttonindex = 1, #PotionBarFloating.Buttons do
        local button = PotionBarFloating.Buttons[buttonindex]
        if button ~= nil and button.slotNum ~= nil and button.itemData ~= nil then
            if (potionBarType ~= PotionBar.Type.UNKNOWNS and button.potionBarType == potionBarType)
                or (potionBarType == PotionBar.Type.UNKNOWNS and string.match(WStringToString(button.itemData.name), "(.*)^") == potionName)
            then
                local hasCoolDown = button.cooldownTimeLeft ~= nil and button.cooldownTimeLeft > 0
                local resultString = ""
                if not hasCoolDown then
                    SendUseItem(GameData.ItemLocs.INVENTORY, button.slotNum, 0, 0, 0)
                    --resultString = PotionBar.LocalizableStrings.MacroPotionFound1 .. string.match(WStringToString(button.itemData.name), "(.*)^") .. PotionBar.LocalizableStrings.MacroPotionFound2 .. button.slotNum .. PotionBar.LocalizableStrings.MacroPotionFound3
                    resultString = PotionBar.LocalizableStrings.MacroPotionFound1 .. (WStringToString(button.itemData.name)) .. PotionBar.LocalizableStrings.MacroPotionFound2 .. button.slotNum .. PotionBar.LocalizableStrings.MacroPotionFound3
                else
                    --resultString = PotionBar.LocalizableStrings.MacroPotionOnCoolDown1 .. string.match(WStringToString(button.itemData.name), "(.*)^") .. PotionBar.LocalizableStrings.MacroPotionOnCoolDown2
                    resultString = PotionBar.LocalizableStrings.MacroPotionOnCoolDown1 .. (WStringToString(button.itemData.name)) .. PotionBar.LocalizableStrings.MacroPotionOnCoolDown2
                end
                TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString(resultString))
                return not hasCoolDown
            end
        end
    end

    TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, PotionBar.LocalizableStrings.MacroNoPotionFound1)
    local errorstring = PotionBar.LocalizableStrings.MacroNoPotionFound2
    if potionBarType == PotionBar.Type.UNKNOWNS then
        errorstring = errorstring .. potionName
    else
        errorstring = errorstring .. PotionBar.Type[potionType].Name
    end
    errorstring = errorstring .. PotionBar.LocalizableStrings.MacroNoPotionFound3
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString(errorstring))
    return false
end

function PotionBarFloating.RButtonUp(buttonId, flags) end
function PotionBarFloating.LButtonDown(buttonId, flags) end

function PotionBarFloating.ActivatorMouseOver()
    if isMoving then return end

    Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name)
    Tooltips.SetTooltipText(1, 1, L"PotionBar")
    Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
    Tooltips.SetTooltipText(2, 1, PotionBar.LocalizableStrings.FloatHint1)
    Tooltips.SetTooltipText(3, 1, PotionBar.LocalizableStrings.FloatHint2)
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( { Point = "topleft", RelativeTo = SystemData.ActiveWindow.name, RelativePoint = "bottomleft", XOffset = 0, YOffset = -10 } )
end

function PotionBarFloating.ActivatorLButtonDown() isMoving = true end
function PotionBarFloating.ActivatorLButtonUp() isMoving = false end

function PotionBarFloating.ActivatorRButtonUp()
    EA_Window_ContextMenu.CreateContextMenu("PotionBarFloatingActivator")
    local movable = WindowGetMovable(EA_Window_ContextMenu.activeWindow)
    EA_Window_ContextMenu.AddMenuItem(GetString(StringTables.Default.LABEL_TO_LOCK), PotionBarFloating.SetMovable, not movable, true)
    EA_Window_ContextMenu.AddMenuItem(GetString(StringTables.Default.LABEL_TO_UNLOCK), PotionBarFloating.SetMovable, movable, true)
    EA_Window_ContextMenu.AddMenuItem(PotionBar.LocalizableStrings.Config, PotionBarFloating.ShowSettings, false, true)
    EA_Window_ContextMenu.Finalize()
end

function PotionBarFloating.ShowSettings() WindowSetShowing("PotionBarWindowConfig", true) end
function PotionBarFloating.SetMovable() WindowSetMovable(EA_Window_ContextMenu.activeWindow, not WindowGetMovable(EA_Window_ContextMenu.activeWindow)) end

function PotionBarFloating.AddButton(potion)
    if PotionBarFloating.Buttons == nil then
        PotionBarFloating.Buttons = {}
    end
    local buttonIndex = 1 + #PotionBarFloating.Buttons
    if PotionBarFloating.Buttons[buttonIndex] == nil then
        PotionBarFloating.Buttons[buttonIndex] = {}
    end
    PotionBarFloating.Buttons[buttonIndex] = potion
end

-- returns true, if a cooldown has ended
local function UpdateCooldown(buttonName, cooldownTotal, cooldownLeft)
    local overlayWindow = buttonName .. "Cooldown"
    if cooldownLeft ~= nil and cooldownTotal ~= nil then
				local overlayText = buttonName .. "CooldownTimer"
		
				if not WindowGetShowing(overlayWindow) then
					WindowSetShowing(overlayWindow, true)
					WindowSetTintColor(overlayWindow, 206, 44, 44)
					LabelSetTextColor(overlayText, 243, 243, 16)
				end
		
				local alpha = cooldownLeft/cooldownTotal
        if alpha > 0.8 then alpha = 0.8 end
        if alpha < 0.3 then alpha = 0.3 end
        WindowSetAlpha(overlayWindow, alpha)
		
        LabelSetText(overlayText, TimeUtils.FormatTimeCondensedTruncate(cooldownLeft))
        
        return false
    else
        if true == WindowGetShowing(overlayWindow) then
            WindowSetShowing(overlayWindow, false)
            return true
        else
            return false
        end
    end
end
local function UpdateButton(buttonIndex)
    local buttonName = "PotionBarButton" .. buttonIndex

    -- might need to create the button first
    if not DoesWindowExist(buttonName) then CreateWindowFromTemplate(buttonName, "PotionBarButtonTemplate", "PotionBar") end

    -- update anchors
    local lastbuttonName = "PotionBarButton" .. buttonIndex - 1
    WindowClearAnchors(buttonName)
    if PotionBar.Settings.Build == PotionBar.Build.Left then
        if buttonIndex == 1 then
            if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
                WindowAddAnchor(buttonName, "bottomleft", "PotionBarFloatingActivator", "bottomright", 0, 0)
            else
                WindowAddAnchor(buttonName, "topleft", "PotionBarFloatingActivator", "topright", 0, 0)
            end
        else
            WindowAddAnchor(buttonName, "left", lastbuttonName, "right", 0, 0)
        end
    elseif PotionBar.Settings.Build == PotionBar.Build.Up then
        if buttonIndex == 1 then
            if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
                WindowAddAnchor(buttonName, "topright", "PotionBarFloatingActivator", "bottomright", 0, 0)
            else
                WindowAddAnchor(buttonName, "topleft", "PotionBarFloatingActivator", "bottomleft", 0, 0)
            end
        else
            WindowAddAnchor(buttonName, "top", lastbuttonName, "bottom", 0, 0)
        end
    elseif PotionBar.Settings.Build == PotionBar.Build.Down then
        if buttonIndex == 1 then
            if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
                WindowAddAnchor(buttonName, "bottomright", "PotionBarFloatingActivator", "topright", 0, 0)
            else
                WindowAddAnchor(buttonName, "bottomleft", "PotionBarFloatingActivator", "topleft", 0, 0)
            end
        else
            WindowAddAnchor(buttonName, "bottom", lastbuttonName, "top", 0, 0)
        end
    else
        -- PotionBar.Settings.Build == PotionBar.Build.Right
        if buttonIndex == 1 then
            if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
                WindowAddAnchor(buttonName, "bottomright", "PotionBarFloatingActivator", "bottomleft", 0, 0)
            else
                WindowAddAnchor(buttonName, "topright", "PotionBarFloatingActivator", "topleft", 0, 0)
            end
        else
            WindowAddAnchor(buttonName, "right", lastbuttonName, "left", 0, 0)
        end
    end

    -- set texture
    local itemData = PotionBarFloating.Buttons[buttonIndex].itemData
    local texture, x, y = GetIconData(itemData.iconNum)
    local iconWindowName = buttonName .. "Icon"
    DynamicImageSetTextureDimensions(iconWindowName, x, y)
    DynamicImageSetTexture(iconWindowName, texture, x, y)

    -- draw cooldown
    UpdateCooldown(buttonName, PotionBarFloating.Buttons[buttonIndex].totalCooldownTime, PotionBarFloating.Buttons[buttonIndex].cooldownTimeLeft)

    -- update labels
    local stackCount = itemData.stackCount
    local totalCount = PotionBarFloating.Buttons[buttonIndex].totalTypeCount
    local text = L""
    if stackCount == totalCount and not PotionBar.Settings.ShowStackSizeEvenIfLast then
        totalCount = 0
    end
    local maintext = L""
    if PotionBar.Settings.TopRight == PotionBar.StackCountInfoType.Total and totalCount ~= 0 and (totalCount > 1 or PotionBar.Settings.ShowStackSizeEvenIf1) then
        maintext = StringToWString(tostring(totalCount))
    elseif PotionBar.Settings.TopRight == PotionBar.StackCountInfoType.Single and stackCount ~= 0 and (stackCount > 1 or PotionBar.Settings.ShowStackSizeEvenIf1) then
        maintext = StringToWString(tostring(stackCount))
    end
    LabelSetText(buttonName .. "Text", maintext)
    local subtext = L""
    if PotionBar.Settings.BottomRight == PotionBar.StackCountInfoType.Total and totalCount ~= 0 and (totalCount > 1 or PotionBar.Settings.ShowStackSizeEvenIf1) then
        subtext = StringToWString(tostring(totalCount))
    elseif PotionBar.Settings.BottomRight == PotionBar.StackCountInfoType.Single and stackCount ~= 0 and (stackCount > 1 or PotionBar.Settings.ShowStackSizeEvenIf1) then
        subtext = StringToWString(tostring(stackCount))
    end
    -- subtext = StringToWString(tostring(slotNum)) -- for debugging purposes
    LabelSetText(buttonName .. "SubText", subtext)
end

local oldButtonCount = 0
local function UpdateActionBar()
    PotionBar.Inventory.needsUIRefresh = false
    for buttonIndex, _ in ipairs(PotionBarFloating.Buttons) do
        UpdateButton(buttonIndex)
    end
    if oldButtonCount > #PotionBarFloating.Buttons then
        for buttonIndex = #PotionBarFloating.Buttons + 1, oldButtonCount do
            if DoesWindowExist("PotionBarButton" .. buttonIndex) then
                DestroyWindow("PotionBarButton" .. buttonIndex)
            end
        end
    end
    oldButtonCount = #PotionBarFloating.Buttons
end
function PotionBarFloating.ReflowButtons()
    local numRows = 1
    local numColumns = #PotionBarFloating.Buttons
    local border = 5 * 2
    local horizontalPad = 2 * (numColumns - 1)
    local verticalPad = 2 * 0
    local buttonSize = 48

    WindowClearAnchors("PotionBar")

    if PotionBar.Settings.Build == PotionBar.Build.Left then
        numRows = 1
        numColumns = #PotionBarFloating.Buttons
        if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
            WindowAddAnchor("PotionBar", "bottomleft", "PotionBarFloatingActivator", "bottomright", 0, 0)
        else
            WindowAddAnchor("PotionBar", "topleft", "PotionBarFloatingActivator", "topright", 0, 0)
        end
    elseif PotionBar.Settings.Build == PotionBar.Build.Up then
        numRows = #PotionBarFloating.Buttons
        numColumns = 1
        if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
            WindowAddAnchor("PotionBar", "topright", "PotionBarFloatingActivator", "bottomright", 0, 0)
        else
            WindowAddAnchor("PotionBar", "topleft", "PotionBarFloatingActivator", "bottomleft", 0, 0)
        end
    elseif PotionBar.Settings.Build == PotionBar.Build.Down then
        numRows = #PotionBarFloating.Buttons
        numColumns = 1
        if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
            WindowAddAnchor("PotionBar", "bottomright", "PotionBarFloatingActivator", "topright", 0, 0)
        else
            WindowAddAnchor("PotionBar", "bottomleft", "PotionBarFloatingActivator", "topleft", 0, 0)
        end
    else
        -- PotionBar.Settings.Build == PotionBar.Build.Right
        numRows = 1
        numColumns = #PotionBarFloating.Buttons
        if PotionBar.Settings.Activator == PotionBar.ActivatorModes.RightOrBottom then
            WindowAddAnchor("PotionBar", "bottomright", "PotionBarFloatingActivator", "bottomleft", 0, 0)
        else
            WindowAddAnchor("PotionBar", "topright", "PotionBarFloatingActivator", "topleft", 0, 0)
        end
    end

    local fudgeHeight = 6 * numRows
    local fudgeWidth = 5 * numColumns

    local width = (numColumns * buttonSize) + horizontalPad + border + fudgeWidth
    local height = (numRows * buttonSize) + verticalPad + border + fudgeHeight

    WindowSetDimensions("PotionBar", width, height)
    WindowSetShowing("PotionBar", PotionBarFloating.Buttons[1] ~= nil)
    WindowSetLayer("PotionBar", Window.Layers.DEFAULT)
    WindowSetShowing("PotionBarButtonsBackground", not(noBackground))

    UpdateActionBar()

    PotionBarFloating.Scale()
    PotionBarFloating.Alpha()
end

local autohideTimer = 0
local autohideDelay = 3
function PotionBarFloating.RefreshButtons(timeElapsed)
    if PotionBar.Inventory.needsUIRefresh then
		local bNeedsUpdateButtons = false
        PotionBar.Inventory.needsUIRefresh = false
        for buttonIndex, potion in pairs(PotionBarFloating.Buttons) do
            if true == UpdateCooldown("PotionBarButton" .. buttonIndex, potion.totalCooldownTime, potion.cooldownTimeLeft) then
				bNeedsUpdateButtons = true
				break
			end
        end
		if bNeedsUpdateButtons then PotionBar.UpdateButtons() end
    end

    if PotionBar.Settings.Autohide == true then
        if autohideTimer > autohideDelay then
            autohideTimer = 0
            PotionBarFloating.Autohide()
        elseif timeElapsed then
			autohideTimer = autohideTimer + timeElapsed
        end
    end
end

function PotionBarFloating.Autohide()
    if PotionBar.Settings.Autohide == true and #PotionBarFloating.Buttons > 0 and not(string.find(SystemData.MouseOverWindow.name, 'PotionBar')) then
        for index in ipairs(PotionBarFloating.Buttons) do WindowSetShowing("PotionBarButton" .. index, false) end
    end
end

function PotionBarFloating.Autoshow()
    for index in ipairs(PotionBarFloating.Buttons) do WindowSetShowing("PotionBarButton" .. index, true) end
end

function PotionBarFloating.Scale()
    local scale = PotionBar.Settings.Scale + .375
    WindowSetScale("PotionBarFloatingActivator", scale)
    WindowSetScale("PotionBar", scale)
    for index in ipairs(PotionBarFloating.Buttons) do WindowSetScale("PotionBarButton" .. index, scale) end
end

function PotionBarFloating.Alpha()
    local alpha = PotionBar.Settings.Opacity
	if alpha ~= 0 then
		
		WindowSetAlpha("PotionBarFloatingActivator", alpha)
		WindowSetAlpha("PotionBar", alpha)

		-- Hide activator based on config settings
		if ( PotionBar.Settings.Activator == 3 ) then
				WindowSetShowing("PotionBarFloatingActivator", false)
		else
				WindowSetShowing("PotionBarFloatingActivator", true)
		end

		WindowSetShowing("PotionBar", true)

		for index in ipairs(PotionBarFloating.Buttons) do
			WindowSetAlpha("PotionBarButton" .. index, alpha)
			WindowSetAlpha("PotionBarButton" .. index .. "CooldownTimer", alpha)
			WindowSetAlpha("PotionBarButton" .. index .. "Text", alpha)
			WindowSetAlpha("PotionBarButton" .. index .. "SubText", alpha)
			WindowSetShowing("PotionBarButton" .. index, true)
			WindowSetShowing("PotionBarButton" .. index .. "CooldownTimer", true)
			WindowSetShowing("PotionBarButton" .. index .. "Text", true)
			WindowSetShowing("PotionBarButton" .. index .. "SubText", true)
		end
		
	else
		
		WindowSetShowing("PotionBarFloatingActivator", false)
		WindowSetShowing("PotionBar", false)

		for index in ipairs(PotionBarFloating.Buttons) do
			WindowSetShowing("PotionBarButton" .. index, false)
			WindowSetShowing("PotionBarButton" .. index .. "CooldownTimer", false)
			WindowSetShowing("PotionBarButton" .. index .. "Text", false)
			WindowSetShowing("PotionBarButton" .. index .. "SubText", false)
		end

	end
end
