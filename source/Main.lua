if not PotionBar then PotionBar = {} end

PotionBar.Inventory = {}

local ipairs = ipairs
local bIsUpdating = false

-- Specific information for looking up RvR currency bought potions
local uniqueIdLookup = {
	-- RvR Potions
	[208251] = PotionBar.Type.HEAL_RVR,	-- "Recruit's Potion of Aid"
	[208252] = PotionBar.Type.AP_RVR,		-- "Recruit's Warming Potion"
	[208261] = PotionBar.Type.HEAL_RVR,	-- "Scout's Recovery Potion"
	[208262] = PotionBar.Type.AP_RVR,		-- "Scout's Quickening Potion"
	[208271] = PotionBar.Type.HEAL_RVR,	-- "Soldier's Healing Draught"
	[208272] = PotionBar.Type.AP_RVR,		-- "Soldier's Stimulant Potion"
	[208281] = PotionBar.Type.HEAL_RVR,	-- "Officer's Healing Elixir"
	[208282] = PotionBar.Type.AP_RVR,		-- "Officer's Phial of Adrenaline"
	[208291] = PotionBar.Type.HEAL_RVR,	-- "Officer's Elixir of Rejuvenation"
	[208292] = PotionBar.Type.AP_RVR,		-- "Officer's Alacritious Elixir"
	[208293] = PotionBar.Type.HEAL_RVR,	-- "Captain's Elixir of Rejuvenation"
	[208294] = PotionBar.Type.AP_RVR,		-- "Captain's Alacritious Elixir"
	-- Liniments
	[197910] =  PotionBar.Type.WARBLOOD,						-- Liniment: War Blood
	[197909] =  PotionBar.Type.WARDEMISE,						-- Liniment: War Demise
	[197912] =  PotionBar.Type.WARFERVOR,						-- Liniment: War Fervor
	[197911] =  PotionBar.Type.WARGENIUS,						-- Liniment: War Genius
	[197914] =  PotionBar.Type.WARHUNGER,						-- Liniment: War Hunger
	[197913] =  PotionBar.Type.WARMERCY,						-- Liniment: War Mercy
	[197917] =  PotionBar.Type.BOUNDLESSSIGHT,			-- Liniment: Boundless Sight
	[197919] =  PotionBar.Type.IMMUTABLEDEFIANCE,		-- Liniment: Immutable Defiance
	[197915] =  PotionBar.Type.INEXORABLEAEGIS,			-- Liniment: Inexorable Aegis
	[197905] =  PotionBar.Type.INSPIRATIONALWINDS,	-- Liniment: Inspirational Winds
	[197918] =  PotionBar.Type.PEERLESSDEFENSE,			-- Liniment: Peerless Defense
	[197916] =  PotionBar.Type.QUICKENEDBLADES,			-- Liniment: Quickened Blades
	[197903] =  PotionBar.Type.SAVAGEVIGOR,					-- Liniment: Savage Vigor
	[197920] =  PotionBar.Type.SWIFTTERGIVERSATION,	-- Liniment: Swift Tergiversation
	[197908] =  PotionBar.Type.ETERNALHUNT,					-- Liniment: Eternal Hunt
	[197904] =  PotionBar.Type.INEVITABLETEMPEST,		-- Liniment: Inevitable Tempest
	[197907] =  PotionBar.Type.TOLLINGBELL,					-- Liniment: Tolling Bell
	[197906] =  PotionBar.Type.UNFETTEREDZEAL,			-- Liniment: Unfettered Zeal
}

function PotionBar.getValues(id, level, name, uniqueID)
    name = WStringToString(name)
    local description = WStringToString(GetAbilityDescription(id, level))

		-- Handle special case lookups
		local lookup = uniqueIdLookup[uniqueID]
		if lookup then
			if uniqueID >= 197903 or uniqueID <= 197920 then	-- "IsLiniment()"
				return 100, 1800, lookup
			end
			return PotionBar.getNumbers(description, 1),0,lookup	-- this only works for the rvr potions currently ofc
		end

    if SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN then
        return PotionBar.getValuesDE(name, description)
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.FRENCH then
        return PotionBar.getValuesFR(name, description)
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.SPANISH then
        return PotionBar.getValuesSP(name, description)
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.ITALIAN then
        return PotionBar.getValuesIT(name, description)
    else
        -- SystemData.Settings.Language.active == SystemData.Settings.Language.ENGLISH
        -- Language unknown or using english (sorry for this arrogance)
        return PotionBar.getValuesEN(name, description)
    end
end

-- parses a string for its numbers and returns each consecutive number (might give multiple returns)
function PotionBar.getNumbers(aString, count)
    if not count then return end
    local x, y, f = 0, 0, false
    local results = {}
    for i = 1, count do
        if i > 1 then y = y + 1 end
        x, y = string.find(aString, '%d+', y)
        results[i] = tonumber(string.sub(aString, x, y))
    end
    return unpack(results)
end

-- returns the first number which it finds in the window under the cursor
function PotionBar.getNumberFromWindowName()
    local windowname = SystemData.MouseOverWindow.name
    local windownumber = tonumber(string.sub(windowname, string.find(windowname, '%d+')))
    return windownumber
end

function PotionBar.Use(args)
    if type(args) == "wstring" then
        args = WStringToString(args)
    end
    if type(args) ~= "string" then return false end
    return PotionBar.LibSlashHandler("use " .. args)
end

function PotionBar.LibSlashHandler(args)
    local bUsedPotion = false
    if not args then return bUsedPotion end

    local command = PotionBar.LibSlashCommand
    if command == nil then command = "PotionBar" end

    -- accept params like this:
    -- /potionbar help
    -- /potionbar config
    -- /potionbar reset
    -- /potionbar use AP
    -- /potionbar use HEAL
    -- ...
    -- /potionbar use UNKNOWN 'Name Of Potion'

    local invalidparams = true
    local argv = StringSplit(args)

    if argv ~= nil and #argv >= 1 then
        if string.lower(argv[1]) == "config" then
            WindowSetShowing("PotionBarWindowConfig", true)
            invalidparams = false
        elseif string.lower(argv[1]) == "reset" then
            PotionBar.Defaults()
            invalidparams = false
        elseif string.lower(argv[1]) == "use" and #argv >= 2 and argv[2] ~= nil then
            local useType = nil
            local useName = nil
            argv[2] = string.upper(argv[2])
            for index, Type in ipairs(PotionBar.Type) do
                if Type.Name == argv[2] then
                    useType = index
                    break
                end
            end
            if useType == PotionBar.Type.UNKNOWNS and #argv >= 3 and argv[3] ~= nil then
                useName = string.match(args, "'.*'")
                if useName == nil then
                    useName = string.match(args, "\".*\"") -- for compatibility
                end
                if useName ~= nil and #useName >= 2 then
                    useName = string.sub(useName, 2, #useName-1)
                end
            end
            invalidparams = useType == nil or (useType == PotionBar.Type.UNKNOWNS and useName == nil)
            if not invalidparams then
                bUsedPotion = PotionBarFloating.UsePotion(useType, useName)
                PotionBar.UpdateButtons(true)
            end
        end
    end

    if invalidparams then
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, PotionBar.LocalizableStrings.HelpTitle)
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/" .. command .. " help"))
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/" .. command .. " config"))
        for Type = 1, #PotionBar.Type do
            if Type ~= PotionBar.Type.UNKNOWNS then
                TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/" .. command .. " use " .. PotionBar.Type[Type].Name))
            end
        end
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, PotionBar.LocalizableStrings.HelpForUnknown)
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/" .. command .. " use " .. PotionBar.Type[PotionBar.Type.UNKNOWNS].Name .. " '" .. PotionBar.LocalizableStrings.HelpNameOfPotion .. "'"))
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, PotionBar.LocalizableStrings.HelpForMacros)
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/script PotionBar.Use(\"" .. PotionBar.Type[PotionBar.Type.HEAL].Name .. "\")"))
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, StringToWString("/script PotionBar.Use(\"" .. PotionBar.Type[PotionBar.Type.UNKNOWNS].Name .. " '" .. PotionBar.LocalizableStrings.HelpNameOfPotion .. "'\")"))
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"")
    end
    return bUsedPotion
end

function PotionBar.Initialize()
    if PotionBar.Settings == nil then PotionBar.Defaults() end

    local currentVersion = tonumber(PotionBar.CurrentMajorVersion..L"."..PotionBar.CurrentMinorVersion)
    if PotionBar.Settings.Version ~= currentVersion then PotionBar.Defaults() end

    local I18N = {}
    if SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN then
        I18N = PotionBar.German
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.FRENCH then
        I18N = PotionBar.French
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.SPANISH then
        I18N = PotionBar.Spanish
    elseif SystemData.Settings.Language.active == SystemData.Settings.Language.ITALIAN then
        I18N = PotionBar.Italian
    else
        -- SystemData.Settings.Language.active == SystemData.Settings.Language.ENGLISH
        -- Language Unknown or English, please excuse my arrogance
        I18N = PotionBar.English
    end

    -- I18N = PotionBar.English -- uncomment for screen shots

    if I18N ~= nil then
        for i = 1, #PotionBar.Priorities do
            if I18N.Priorities == nil then
                break
            end
            if I18N.Priorities[i] ~= nil then
                PotionBar.Priorities[i].Desc = I18N.Priorities[i].Desc
            end
        end
        for i = 1, #PotionBar.Build do
            if I18N.Build == nil then
                break
            end
            if I18N.Build[i] ~= nil then
                PotionBar.Build[i].Desc = I18N.Build[i].Desc
            end
        end
        for i = 1, #PotionBar.ActivatorModes do
            if I18N.ActivatorModes == nil then
                break
            end
            if I18N.ActivatorModes[i] ~= nil then
                PotionBar.ActivatorModes[i].Desc = I18N.ActivatorModes[i].Desc
            end
        end
        for i = 1, #PotionBar.Type do
            if I18N.Type == nil then
                break
            end
            if I18N.Type[i] ~= nil then
                PotionBar.Type[i].Desc = I18N.Type[i].Desc
            end
        end
        for i = 1, #PotionBar.StackCountInfoType do
            if I18N.StackCountInfoType == nil then
                break
            end
            if I18N.StackCountInfoType[i] ~= nil then
                PotionBar.StackCountInfoType[i].Desc = I18N.StackCountInfoType[i].Desc
            end
        end
        for key, value in pairs(PotionBar.LocalizableStrings) do
            if I18N.Strings == nil then
                break
            end
            if I18N.Strings[key] ~= nil then
                PotionBar.LocalizableStrings[key] = I18N.Strings[key]
            end
        end
    end

    CreateWindow("PotionBarFloatingActivator", false)
    CreateWindow("PotionBar", false)
    CreateWindow("PotionBarWindowConfig", false)

    if LibSlash ~= nil and LibSlash.RegisterSlashCmd ~= nil then
        if LibSlash.RegisterSlashCmd("PotionBar", function(args) PotionBar.LibSlashHandler(args) end) then
            PotionBar.LibSlashCommand = "PotionBar"
        end
    end

    WindowClearAnchors("PotionBarFloatingActivator")
    if PotionBar.Settings.lastAnchor ~= nil and PotionBar.Settings.lastAnchor.x ~= nil and PotionBar.Settings.lastAnchor.y ~= nil then
        WindowAddAnchor("PotionBarFloatingActivator", "topleft", "Root", "topleft", PotionBar.Settings.lastAnchor.x, PotionBar.Settings.lastAnchor.y)
    else
        WindowAddAnchor("PotionBarFloatingActivator", "center", "Root", "center", 0, 0)
    end
    
    -- Register events that force an update
    RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "PotionBar.InventorySlotUpdated")
    RegisterEventHandler(SystemData.Events.PLAYER_BATTLE_LEVEL_UPDATED, "PotionBar.OnPlayerBattleLevelUpdated")
    RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "PotionBar.OnPlayerRenownRankUpdated")
    
		PotionBar.UpdateButtons(true)

		-- Output that PotionBar has most likely initialized
		TextLogAddEntry( "Chat", SystemData.ChatLogFilters.SHOUT, L"[PotionBar]: "..L"Version "..(wstring.format( L"%.01f",PotionBar.Settings.Version))..L" Initialized" )
		DEBUG ( towstring( "PotionBar loaded" ))
end

function PotionBar.Shutdown()
    if LibSlash ~= nil and LibSlash.UnregisterSlashCmd ~= nil and PotionBar.LibSlashCommand ~= nil then
        LibSlash.UnregisterSlashCmd(PotionBar.LibSlashCommand)
    end
    PotionBar.LibSlashCommand = nil
    PotionBar.Name = {}
    PotionBar.Description = {}
    PotionBar.Potions = {}
    PotionBar.Inventory = {}
    PotionBarFloating.Buttons = {}

    local x, y = WindowGetOffsetFromParent("PotionBarFloatingActivator")
    local windowscale = WindowGetScale("PotionBarFloatingActivator") / InterfaceCore.GetScale()
    PotionBar.Settings.lastAnchor = {}
    PotionBar.Settings.lastAnchor.x = x * windowscale
    PotionBar.Settings.lastAnchor.y = y * windowscale

		-- Destroy Windows
    DestroyWindow("PotionBarFloatingActivator")
    DestroyWindow("PotionBar")
    DestroyWindow("PotionBarWindowConfig")

    -- Unregister Eventhandlers
    UnRegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "PotionBar.InventorySlotUpdated")
    UnRegisterEventHandler(SystemData.Events.PLAYER_BATTLE_LEVEL_UPDATED, "PotionBar.OnPlayerBattleLevelUpdated")
    UnRegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "PotionBar.OnPlayerRenownRankUpdated")
end

function PotionBar.Show() 
		WindowSetShowing("PotionBarButtons", true) 
end

function PotionBar.Hide() 
		WindowSetShowing("PotionBarButtons", false) 
end

function PotionBar.InventorySlotUpdated(updatedSlots)
		local bIsUpdating = true
		PotionBar.UpdateButtonsQueue()
end

function PotionBar.OnPlayerBattleLevelUpdated()
		local bIsUpdating = true
		PotionBar.UpdateButtonsQueue()
end

function PotionBar.OnPlayerRenownRankUpdated()
		local bIsUpdating = true
		PotionBar.UpdateButtonsQueue()
end

-- Function that avoids possible race condition if two events occur simultaneously
function PotionBar.UpdateButtonsQueue()
		if not bIsUpdating then
				PotionBar.UpdateButtons(true)
		end
end

-- update the cool down timers
-- if we set PotionBar.Inventory.needsUIRefresh to true then PotionBarFloating.RefreshButtons will update the buttons on OnUpdate-event
local refreshTimer = 0
local refreshDelay = 1
function PotionBar.UpdateCooldowns(timeElapsed)
    if not timeElapsed then return end
    if PotionBar.Inventory == nil or PotionBar.Inventory.countOfPotionsWithCooldown == nil or PotionBar.Inventory.countOfPotionsWithCooldown  <= 0 then return end

    refreshTimer = refreshTimer + timeElapsed

    for _, potion in ipairs(PotionBar.Inventory) do
        if potion ~= nil and potion.cooldownTimeLeft ~= nil and potion.totalCooldownTime ~= nil then
            potion.cooldownTimeLeft = potion.cooldownTimeLeft - timeElapsed
            if potion.cooldownTimeLeft <= 0 then
                potion.cooldownTimeLeft = nil
                potion.totalCooldownTime = nil
                PotionBar.Inventory.countOfPotionsWithCooldown = PotionBar.Inventory.countOfPotionsWithCooldown - 1
                PotionBar.Inventory.needsUIRefresh = true
            end
        end
    end

    PotionBar.Inventory.needsUIRefresh = (PotionBar.Inventory.needsUIRefresh
                                          or PotionBar.Inventory.countOfPotionsWithCooldown == 0
                                          or refreshTimer >= refreshDelay)

    if PotionBar.Inventory.needsUIRefresh then refreshTimer = 0 end
end

--
-- UpdateButtons Part
--
local function DurationPrecalculatedIntoPower(potionBarType)
    return potionBarType == PotionBar.Type.SHIELD
           or potionBarType == PotionBar.Type.REGEN
           or potionBarType == PotionBar.Type.DOT
           or potionBarType == PotionBar.Type.AOEDOT
           or potionBarType == PotionBar.Type.FIREBREATH
end

local function CalcPowerByDuration(potionBarType, potion)
    if potion.value == nil or potion.value == 0 then return 0 end
    if potion.duration == nil or potion.duration == 0 then return potion.value end
    if potionBarType == PotionBar.Type.SHIELD then
        return potion.duration / potion.value
    end
    if potionBarType == PotionBar.Type.REGEN
       or potionBarType == PotionBar.Type.DOT
       or potionBarType == PotionBar.Type.AOEDOT
       or potionBarType == PotionBar.Type.FIREBREATH
    then
        return potion.value / potion.duration
    end
    return potion.value
end

-- comparision function for quicksort
local function IsBackPackItemLessThan(leftItem, rightItem)

    if leftItem.potionBarType ~= rightItem.potionBarType then
        return leftItem.buttonPosition or 0 < rightItem.buttonPosition or 0
    end

    local itemtype = leftItem.potionBarType
    
    if itemtype == PotionBar.Type.UNKNOWNS then
        if WStringsCompare(leftItem.itemData.name, rightItem.itemData.name) ~= 0 then
            return leftItem.itemData.name < rightItem.itemData.name
        end
    end

    local preference = leftItem.preference

    if preference == PotionBar.Priorities.BigStack then
        if leftItem.itemData.stackCount == rightItem.itemData.stackCount then
            return leftItem.slotNum < rightItem.slotNum
        end
        return leftItem.itemData.stackCount > rightItem.itemData.stackCount
    elseif preference == PotionBar.Priorities.LessStack then
        if leftItem.itemData.stackCount == rightItem.itemData.stackCount then
            return leftItem.slotNum < rightItem.slotNum
        end
        return leftItem.itemData.stackCount < rightItem.itemData.stackCount
    end

    local leftPower = CalcPowerByDuration(leftItem.potionBarType, leftItem)
    local rightPower = CalcPowerByDuration(rightItem.potionBarType, rightItem)
    local durationMatters = leftItem.duration ~= nil and rightItem.duration ~= nil and leftPower == rightPower and not DurationPrecalculatedIntoPower(itemtype)

    if preference == PotionBar.Priorities.Weakest then
        if (leftPower < rightPower) or (durationMatters and leftItem.duration < rightItem.duration) then
            return true
        end
    elseif preference == PotionBar.Priorities.Strongest then
        if (leftPower > rightPower) or (durationMatters and leftItem.duration > rightItem.duration) then
            return true
        end
    end

    if leftPower == rightPower and (not durationMatters or leftItem.duration == rightItem.duration) then
        if leftItem.itemData.stackCount < rightItem.itemData.stackCount then
            return true
        end
        if leftItem.itemData.stackCount == rightItem.itemData.stackCount then
            return leftItem.slotNum < rightItem.slotNum
        end
    end

    return false
end

-- from http://code.google.com/p/luainterface/source/browse/trunk/lua-5.1.2/test/sort.lua?spec=svn2&r=2
local function quicksort(x, l, u)
    if l >= u then return end
    local m = math.random(u - (l - 1)) + l - 1 	-- choose a random pivot in range l..u
    x[l], x[m] = x[m], x[l] 										-- swap pivot to first position
    local t = x[l]															-- pivot value
    m = l
    local i = l + 1
    while i <= u do
    		-- invariant: x[l+1..m] < t <= x[m+1..i-1]
        if IsBackPackItemLessThan(x[i], t) then
            m = m + 1
            x[m], x[i] = x[i], x[m]							-- swap x[i] and x[m]
        end
        i = i + 1
    end
    x[l], x[m] = x[m], x[l]											-- swap pivot to a valid place
    quicksort(x, l, m-1)
    quicksort(x, m+1, u)
end

local function UpdatePotionInventory()
    PotionBar.Inventory = {}
    local preferences = {}
    local buttonpositions = {}
    for index, button in ipairs(PotionBar.Settings.Buttons) do
        preferences[button.Type] = button.Preference
        buttonpositions[button.Type] = index
    end
    PotionBar.Inventory.countOfPotionsWithCooldown = 0
    PotionBar.Inventory.needsUIRefresh = true

    for slotNum, itemData in pairs(DataUtils.GetItems()) do
        if itemData ~= nil and itemData.name ~= nil
           and itemData.type ~= nil and itemData.type == GameData.ItemTypes.POTION
           and itemData.bonus ~= nil and #itemData.bonus >= 1 and itemData.bonus[1].type == 3
           and (itemData.level == nil or itemData.level <= (GameData.Player.battleLevel))
           and (itemData.renown == nil or itemData.renown <= (GameData.Player.Renown.curRank))
        then
            local potion = {}
            potion.slotNum = slotNum
            potion.itemData = itemData
            potion.cooldownTimeLeft = nil
            potion.totalCooldownTime = nil
            for _, bonusData in ipairs(itemData.bonus) do
                if bonusData.type == GameDefs.ITEMBONUS_USE then
                    if bonusData.cooldownTimeLeft and bonusData.cooldownTimeLeft > 0 and bonusData.totalCooldownTime and bonusData.totalCooldownTime > 0 then
                        potion.cooldownTimeLeft = bonusData.cooldownTimeLeft
                        potion.totalCooldownTime = bonusData.totalCooldownTime
                        PotionBar.Inventory.countOfPotionsWithCooldown = PotionBar.Inventory.countOfPotionsWithCooldown + 1
                    end
                    break
                end
            end
            potion.value, potion.duration, potion.potionBarType = PotionBar.getValues(itemData.bonus[1].reference, itemData.iLevel, itemData.name, itemData.uniqueID)
            if potion.potionBarType == 0 or potion.potionBarType == nil then
                potion.potionBarType = PotionBar.Type.UNKNOWNS
            end
            potion.preference = preferences[potion.potionBarType]
            potion.buttonPosition = buttonpositions[potion.potionBarType]
            PotionBar.Inventory[#PotionBar.Inventory + 1] = potion
        end
    end
    -- sort inventory
    quicksort(PotionBar.Inventory, 1, #PotionBar.Inventory)
end

function PotionBar.UpdateButtons(bDoUpdateInventory)
		if bDoUpdateInventory == true then UpdatePotionInventory() end
		PotionBarFloating.Buttons = {}
		-- inventory is sorted, so we only need to take the first of it's potionBarType
		local alreadyAdded = {}
		for settingsIndex=1, #PotionBar.Settings.Buttons do
		    if PotionBar.Settings.Buttons[settingsIndex].Use == true then
		        local currentButtonType = PotionBar.Settings.Buttons[settingsIndex].Type
		        local joinDifferentNamed = currentButtonType ~= PotionBar.Type.UNKNOWNS and not PotionBar.Settings.Buttons[settingsIndex].SplitName == true
		        for inventoryindex = 1, #PotionBar.Inventory do
		            local potion = PotionBar.Inventory[inventoryindex]
		            if (currentButtonType == potion.potionBarType and alreadyAdded[potion.itemData.name] == nil)
		               or (currentButtonType == PotionBar.Type.SINGLES and potion.itemData.stackCount == 1) then
		                local countOfThatType = 0
		                local totalCooldownTime, cooldownTimeLeft = potion.totalCooldownTime, potion.cooldownTimeLeft
		                local isOnCooldown = totalCooldownTime ~= nil and cooldownTimeLeft ~= nil
		                local allAreOnCooldown = isOnCooldown
		                for _, otherpotion in ipairs(PotionBar.Inventory) do
		                    local typematches = currentButtonType == otherpotion.potionBarType or currentButtonType == PotionBar.Type.SINGLES
		                    local stacksizematches = currentButtonType ~= PotionBar.Type.SINGLES or otherpotion.itemData.stackCount == 1
		                    if (not joinDifferentNamed and stacksizematches and otherpotion.itemData.name ~= nil and 0 == WStringsCompare(potion.itemData.name, otherpotion.itemData.name))
		                       or (joinDifferentNamed and stacksizematches and typematches) then
		                        countOfThatType = countOfThatType + otherpotion.itemData.stackCount
		                        if allAreOnCooldown then
		                            local totalCooldownTime, cooldownTimeLeft = otherpotion.totalCooldownTime, otherpotion.cooldownTimeLeft
		                            allAreOnCooldown = totalCooldownTime ~= nil and cooldownTimeLeft ~= nil
		                        end
		                    end
		                end
		                potion.totalTypeCount = countOfThatType
		                if not isOnCooldown or (isOnCooldown and allAreOnCooldown) then
		                    PotionBarFloating.AddButton(potion)
		                    if currentButtonType ~= PotionBar.Type.SINGLES then
		                        alreadyAdded[potion.itemData.name] = potion.slotNum
		                    end
		                    if joinDifferentNamed then break end
		                end
		            end
		        end
		    end
		end
		PotionBarFloating.ReflowButtons()

		bIsUpdating = false
		
end

--
-- /UpdateButtons Part
--

-- for debugging purposes, and for those who want to extend the support for other languages
function PotionBar.ListUnknowns()
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"PotionBars list of unknown potions:")
    local count = 0
    for _, itemData in pairs(DataUtils.GetItems()) do
        if itemData ~= nil and itemData.name ~= nil
           and itemData.type ~= nil and itemData.type == GameData.ItemTypes.POTION
           and itemData.bonus ~= nil and #itemData.bonus >= 1 and itemData.bonus[1].type == 3
           and (itemData.level == nil or itemData.level <= (GameData.Player.battleLevel))
					 and (itemData.renown == nil or itemData.renown <= (GameData.Player.Renown.curRank))
        then
            local value, duration, potionBarType = PotionBar.getValues(itemData.bonus[1].reference, itemData.iLevel, itemData.name, itemData.uniqueID)
            if potionBarType == 0 or potionBarType == nil then
                potionBarType = PotionBar.Type.UNKNOWNS
            end
            if potionBarType == PotionBar.Type.UNKNOWNS then
                TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, itemData.name)
                count = count + 1
            end
        end
    end
    if count == 0 then
        TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"Your inventory has no unkown potions.")
    end
end
