if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.Italian = {}

function PotionBar.getValuesIT(name, description)
    local potionV, potionD, potionT, potionC, potionTy = 0, 0, 0, 0, 0
    if string.find(description, 'Ripristina istantaneamente') then
        potionV = PotionBar.getNumbers(description, 1)
        if string.find(description, 'Punti Azione') then
            potionTy = PotionBar.Type.AP
        elseif string.find(description, 'Punti Vita') then
            potionTy = PotionBar.Type.HEAL
        end
    elseif string.find(description, 'Cura %d+ Punti Vita ogni') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Recuperi gradualmente %d+[^%d]+unti Vita') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'danni punti danno verranno assorbiti nei prossimi') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'circondi con una barriera magica che assorbir') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Aumenta ') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        if string.find(description, 'Forza') then
            potionTy = PotionBar.Type.STRENGTH
        elseif string.find(description, 'Acume') then
            potionTy = PotionBar.Type.INTELLIGENCE
        elseif string.find(description, 'Balistica') then
            potionTy = PotionBar.Type.BALLISTIC
        elseif string.find(description, 'Resistenza d') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'Resistenza S') then
            potionTy = PotionBar.Type.SPIRIT
        elseif string.find(description, 'Resistenza E') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'Resistenza C') then
            potionTy = PotionBar.Type.CORPOREAL
        elseif string.find(description, 'Armatura') then
            potionTy = PotionBar.Type.ARMOR
        elseif string.find(description, 'Volont') then
            potionTy = PotionBar.Type.WILLPOWER
        end
    elseif string.find(description, 'Degli acule[^%d]+icoprono') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'Un intruglio volatile che brucia il tuo bersaglio') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Una mistura incendiaria che infligge') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Una mistura malvagia che d[^%d]+uoco a u[^%d]+rea bersaglio di') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * potionD
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Una mistura che esploder[^%d]+ sul terreno per') then
        potionD, potionV, potionT, potionC = PotionBar.getNumbers(description, 4)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Un intruglio viscoso che ricopre un[^%d]+rea bersaglio di') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Una mistura spessa che copre il terreno per') then
        potionD, potionC, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Un liquido incandescente che pu') then
        potionC, potionV, potionD = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.FIREBREATH
    end
    return potionV, potionD, potionTy
end
