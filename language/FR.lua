if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.French = {}

function PotionBar.getValuesFR(name, description)
    local potionV, potionD, potionT, potionC, potionTy = 0, 0, 0, 0, 0
    if string.find(description, 'Restaure instantan') then
        potionV = PotionBar.getNumbers(description, 1)
        if string.find(description, 'Points d') then
            potionTy = PotionBar.Type.AP
        elseif string.find(description, 'PV') then
            potionTy = PotionBar.Type.HEAL
        end
    elseif string.find(description, 'Restaure %d+ toutes les %d+ secondes, pendant %d+ secondes.') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Restitue graduellement %d+ PV') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Absorbe %d+[^%d]+ pendant %d+ secondes.') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Vous entoure d[^%d]+une barri[^%d]+re magique qui absorbe') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Augmente l[^%d]+de %d+ pendant %d+ minutes.') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        if string.find(description, 'force') then
            potionTy = PotionBar.Type.STRENGTH
        elseif string.find(description, 'intelligence') then
            potionTy = PotionBar.Type.INTELLIGENCE
        elseif string.find(description, 'de tir') then
            potionTy = PotionBar.Type.BALLISTIC
        elseif string.find(description, 'endurance') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'spirituelle') then
            potionTy = PotionBar.Type.SPIRIT
        elseif string.find(description, 'mentaire') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'aux[^%d]+ments') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'sistance physiqu') then
            potionTy = PotionBar.Type.CORPOREAL
        elseif string.find(description, 'armure') then
            potionTy = PotionBar.Type.ARMOR
        elseif string.find(description, 'votre volon') then
            potionTy = PotionBar.Type.WILLPOWER
        end
    elseif string.find(description, 'Couvre votre corps d') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'Des pointes magiques couvrent votre armure et') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'M[^%d]+lange volatil qui br') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Mixture br[^%d]+lante infligeant') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Mixture infecte qui br') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * potionD
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Une mixture qui prend feu pendant') then
        potionD, potionV, potionT, potionC = PotionBar.getNumbers(description, 4)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Concoction visqueuse qui recouvre une zone') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Mixture [^%d]+paisse qui recouvre le sol') then
        potionD, potionC, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Poudre incandescente pouvant') then
        potionC, potionV, potionD = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.FIREBREATH
    end
    return potionV, potionD, potionTy
end
